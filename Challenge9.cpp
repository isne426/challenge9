#include <iostream>
#include <list>
#include <vector>

using namespace std;

vector<int> hotPotato(int, int);

int main(){
	
	cout << "***HOT POTATO***" << endl;
		
	int no_player, move ;
	
	vector<int> order;
	cout << "How many player(s)? : ";
	cin >> no_player;
	
	cout << "Moves per round : ";
	cin >> move;																										//take data.
	
	order = hotPotato(no_player, move);
	cout << "The hot potato goes to ";																					//calculate sequence.
	
	for (int i=1; i < no_player-1; i++ ){
		
		if (i==0){
			cout << order.at(i);
		}else{
			cout << " => " << order.at(i);
		}
		
	}
	
	cout << endl << "The player no." << order.at(no_player-1) << " WIN this game!" << endl;										//print out the winner.
	
	
	return 0;
}


vector<int> hotPotato(int players, int moves){
	
	int pointer ;
	list<int> List;
	list<int>::iterator ite;
	vector<int> order;
																																//linked list of players.
	for(int i=1; i <= players; i++ ){
		List.push_back(i);
	}
	
	ite = List.begin();
	pointer = 1;
																				//check if the size was not equal to 1 then need to repeat the following steps.
	while(List.size() != 1){
		for(int i=1; i <= moves; i++){
			if(pointer >= List.size()){
				
				ite = List.begin();
				pointer = 1;
				
			}else{
				ite++;
				pointer++;
			}
			
		}																							//make the pointer pass for the moves time.
		
		order.push_back(*ite);
		List.erase(ite);																			//delete destination one.
		
		if(pointer > List.size()){
			pointer = 1;		
		}
		ite = List.begin();																			//send the pointe to the head in case it out of range.
		
		
		for(int i=1; i < pointer; i++){
			
			ite++;
		}
		
	}
	order.push_back(*List.begin());
	
	return order;
}
